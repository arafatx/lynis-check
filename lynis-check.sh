#! /bin/sh
#lynis-check.sh - EARTH.SOFIBOX.COM
/bin/echo "=============================================================================="
/bin/echo "[lynis] Lynis System Audit is checking system..."

#Global Variables

MYFILENAME="lynis-report.txt"
FINALREPORT="lynis-final-report.txt"
MYHOSTNAME=`/bin/hostname`
MYEMAIL="webmaster@sofibox.com"
WARNING_STATUS="N/A"

/bin/echo "Lynis checked on `date`" > /tmp/$MYFILENAME
/bin/lynis update info >> /tmp/$MYFILENAME
/bin/lynis --quick >> /tmp/$MYFILENAME
/bin/echo "**************************************" >> /tmp/$MYFILENAME
/bin/echo "****************DONE******************" >> /tmp/$MYFILENAME

/bin/sed $'s/[^[:print:]\t]//g' /tmp/$MYFILENAME > /tmp/$FINALREPORT

if (grep -e "Warnings" -e "Exceptions found" /tmp/$MYFILENAME) then
 WARNING_STATUS="WARNING"
else
 WARNING_STATUS="OK"
fi

/bin/mail -s "[lynis][$WARNING_STATUS|N] Lynis System Audit Scan Report @ $MYHOSTNAME" $MYEMAIL < /tmp/$FINALREPORT

/bin/rm -rf /tmp/$FINALREPORT
/bin/rm -rf /tmp/$FILENAME

/bin/echo "[lynis] Scan status: $WARNING_STATUS"
/bin/echo "[lynis] Done checking system. Email is set to be sent to webmaster@sofibox.com"
/bin/echo "=============================================================================="
